##############################################################################
#
#    Inouk Core
#    Copyright (c) 2019  Cyril MORISSE (twitter: @cmorisse)
#
##############################################################################
{
    'name': 'Inouk Core',
    'version': '1.0',
    'category': 'Inouk',
    'description': """
Components common to all Inouk modules.
""",
    'author': "Cyril MORISSE (twitter: @cmorisse)",
    'license': 'OPL-1',
    #'website': '',
    'depends': [
        'base',
        'web'
    ],
    'data': [
        'assets_loader.xml',
        'data/ir_config_parameter.xml',
        'security/ir.model.access.csv',
        'views/res_users_views.xml',
        'wizards/res_users_wizard_views.xml',
        'menu.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'application': True,
}