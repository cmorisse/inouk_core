from odoo import api, fields, models
from odoo.exceptions import MissingError, UserError

USER_TYPE_LIST = [
    ('user', 'User'),
    ('admin', 'Administrator')
]


class InoukResUsersWizard(models.TransientModel):
    _name = 'inouk.res_users_wizard'
    _description = "Inouk - Res Users Wizard"

    name = fields.Char(string="User Name")
    email = fields.Char()
    user_type = fields.Selection(USER_TYPE_LIST, default='user')
    send_email_invitation = fields.Boolean(default=True)

    def btn_launch(self):
        inouk_default_template_id = self.env['ir.config_parameter'].sudo().get_param('inouk.default_%s_template_id' % self.user_type) 
        res_users_obj = self.env['res.users'].browse(int(inouk_default_template_id))
        if res_users_obj:
            new_res_users_obj = res_users_obj.copy({
                'name': self.name,
                'login': self.email,
                'active': True,
                'email': self.email if self.send_email_invitation else None
            })
            if not self.send_email_invitation:
                new_res_users_obj.email = self.email
        else:
            raise UserError("No default %s template found." % self.user_type)